package usecase_test

import (
	"bytes"
	"context"
	"log/slog"
	"testing"
	"time"

	"connectrpc.com/connect"
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/binarygame/microservices/guesses/internal/usecase"
	"gitlab.com/binarygame/microservices/guesses/internal/usecase/mocks"
	"gitlab.com/binarygame/microservices/guesses/pkg/errdefs"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
	roomsv1 "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1"
)

func setupMocks() (
	*mocks.MockGuessesRepository,
	*mocks.MockTelemetryService,
	*mocks.MockTelemetrySpan,
	*mocks.MockRoomsServiceClient,
) {
	mockRepo := new(mocks.MockGuessesRepository)
	mockTel := new(mocks.MockTelemetryService)
	mockSpan := new(mocks.MockTelemetrySpan)
	mockRClient := new(mocks.MockRoomsServiceClient)

	return mockRepo, mockTel, mockSpan, mockRClient
}

var (
	logBuffer bytes.Buffer
	logger    = slog.New(slog.NewTextHandler(&logBuffer, nil))
)

func TestSubmit(t *testing.T) {
	roomID := "wxyz"
	userID := uuid.NewString()
	questionID := uuid.NewString()
	correct := true
	timeTaken := 5000 * time.Millisecond

	t.Run("success", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		guess := &models.Guess{RoomID: roomID, UserID: userID, QuestionID: questionID, Correct: correct}
		score := &models.Score{CorrectAnswers: 4, WrongAnswers: 3, ComboMultiplierIdx: 2}
		leaderboard := &models.Leaderboard{
			RankList: []models.RankElement{
				{
					Score:              100.0,
					UserID:             uuid.NewString(),
					ComboMultiplierIdx: 1,
				},
			},
		}

		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("UserAlreadyAnswered", ctx, guess).Return(false, nil)
		repo.On("GetUserScore", ctx, roomID, userID).Return(score, nil)
		repo.On("StoreGuess", ctx, guess, score).Return(nil)
		repo.On("PublishEvent", mock.Anything, mock.Anything).Return(nil)
		repo.On("GetLeaderboard", ctx, roomID).Return(leaderboard, nil)
		rcl.On("UserFinishedAnswering", mock.Anything, mock.Anything).Return(nil, nil)
		tel.On("IncrementCounter", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int")).Return()

		uc := usecase.New(repo, logger, tel, rcl)
		err := uc.Submit(ctx, roomID, userID, questionID, correct, timeTaken)

		require.NoError(t, err)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("already answered", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		guess := &models.Guess{RoomID: roomID, UserID: userID, QuestionID: questionID, Correct: correct}
		uc := usecase.New(repo, logger, tel, rcl)

		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("UserAlreadyAnswered", ctx, guess).Return(true, nil)

		err := uc.Submit(ctx, roomID, userID, questionID, correct, timeTaken)
		require.ErrorIs(t, err, errdefs.ErrUserAlreadyAnswered)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("database error on UserAlreadyAnswered", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		guess := &models.Guess{RoomID: roomID, UserID: userID, QuestionID: questionID, Correct: correct}
		uc := usecase.New(repo, logger, tel, rcl)

		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("UserAlreadyAnswered", ctx, guess).Return(false, errdefs.ErrDatabaseFailed)

		err := uc.Submit(ctx, roomID, userID, questionID, correct, timeTaken)
		require.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("database error on GetUserScore", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		guess := &models.Guess{RoomID: roomID, UserID: userID, QuestionID: questionID, Correct: correct}
		uc := usecase.New(repo, logger, tel, rcl)

		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("UserAlreadyAnswered", ctx, guess).Return(false, nil)
		repo.On("GetUserScore", ctx, roomID, userID).Return(nil, errdefs.ErrDatabaseFailed)

		err := uc.Submit(ctx, roomID, userID, questionID, correct, timeTaken)
		require.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("score not found", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		guess := &models.Guess{RoomID: roomID, UserID: userID, QuestionID: questionID, Correct: correct}
		uc := usecase.New(repo, logger, tel, rcl)

		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("UserAlreadyAnswered", ctx, guess).Return(false, nil)
		repo.On("GetUserScore", ctx, roomID, userID).Return(nil, nil)

		err := uc.Submit(ctx, roomID, userID, questionID, correct, timeTaken)
		require.ErrorIs(t, err, errdefs.ErrInternalServer)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("database error on StoreGuess", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		guess := &models.Guess{RoomID: roomID, UserID: userID, QuestionID: questionID, Correct: correct}
		score := &models.Score{CorrectAnswers: 4, WrongAnswers: 3, ComboMultiplierIdx: 2}
		uc := usecase.New(repo, logger, tel, rcl)

		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("UserAlreadyAnswered", ctx, guess).Return(false, nil)
		repo.On("GetUserScore", ctx, roomID, userID).Return(score, nil)
		repo.On("StoreGuess", ctx, guess, score).Return(errdefs.ErrDatabaseFailed)

		err := uc.Submit(ctx, roomID, userID, questionID, correct, timeTaken)
		require.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})
}

func TestGetLeaderboard(t *testing.T) {
	roomID := "wxyz"

	t.Run("success", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		leaderboard := &models.Leaderboard{
			RankList: []models.RankElement{
				{
					Score:  100.0,
					UserID: uuid.NewString(),
				},
			},
		}
		uc := usecase.New(repo, logger, tel, rcl)

		repo.On("GetLeaderboard", ctx, roomID).Return(leaderboard, nil)

		result, err := uc.GetLeaderboard(ctx, roomID)
		require.NoError(t, err)
		require.Equal(t, leaderboard, result)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("database error", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		uc := usecase.New(repo, logger, tel, rcl)
		repo.On("GetLeaderboard", ctx, roomID).Return(nil, errdefs.ErrDatabaseFailed)

		_, err := uc.GetLeaderboard(ctx, roomID)
		require.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})
}

func TestResetScores(t *testing.T) {
	roomID := "xyz"
	usersInRoom := []string{"a1", "b2"}
	qCount := uint32(10)

	t.Run("success", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		uc := usecase.New(repo, logger, tel, rcl)
		repo.On("ResetRoomScores", ctx, roomID, usersInRoom, qCount).Return(nil)
		rcl.On("GetUsersInRoom", mock.Anything, mock.Anything).Return(
			&connect.Response[roomsv1.GetUsersInRoomResponse]{
				Msg: &roomsv1.GetUsersInRoomResponse{
					UserIds: usersInRoom,
				},
			}, nil)

		err := uc.ResetScores(ctx, roomID, qCount)
		require.NoError(t, err)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("database error", func(t *testing.T) {
		repo, tel, sp, rcl := setupMocks()
		ctx := context.Background()

		uc := usecase.New(repo, logger, tel, rcl)
		repo.On("ResetRoomScores", ctx, roomID, usersInRoom, qCount).Return(errdefs.ErrDatabaseFailed)
		rcl.On("GetUsersInRoom", mock.Anything, mock.Anything).Return(
			&connect.Response[roomsv1.GetUsersInRoomResponse]{
				Msg: &roomsv1.GetUsersInRoomResponse{
					UserIds: usersInRoom,
				},
			}, nil)

		err := uc.ResetScores(ctx, roomID, qCount)
		require.Equal(t, errdefs.ErrDatabaseFailed, err)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})
}
