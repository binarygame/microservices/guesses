package server

import (
	"context"

	"connectrpc.com/connect"
	"gitlab.com/binarygame/microservices/guesses/pkg/errdefs"
	guessesv1 "gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1"
)

func (gs *GuessesServer) SubmitGuess(ctx context.Context, r *connect.Request[guessesv1.SubmitGuessRequest]) (*connect.Response[guessesv1.SubmitGuessResponse], error) {
	err := gs.usecase.Submit(ctx, r.Msg.RoomId, r.Msg.UserId, r.Msg.QuestionId, r.Msg.IsCorrect, r.Msg.TimeTaken.AsDuration())
	if err != nil {
		var connectErr *connect.Error

		switch err {
		case errdefs.ErrDatabaseFailed:
			connectErr = connect.NewError(connect.CodeInternal, err)
		default:
			connectErr = connect.NewError(connect.CodeUnknown, err)
		}

		return nil, connectErr
	}

	return connect.NewResponse(&guessesv1.SubmitGuessResponse{}), nil
}

func (gs *GuessesServer) GetLeaderboard(ctx context.Context, r *connect.Request[guessesv1.GetLeaderboardRequest]) (*connect.Response[guessesv1.GetLeaderboardResponse], error) {
	leaderboard, err := gs.usecase.GetLeaderboard(ctx, r.Msg.RoomId)
	if err != nil {
		var connectErr *connect.Error

		switch err {
		case errdefs.ErrInvalidData:
			connectErr = connect.NewError(connect.CodeInvalidArgument, err)
		case errdefs.ErrUserAlreadyAnswered:
			connectErr = connect.NewError(connect.CodeAlreadyExists, err)
		case errdefs.ErrDatabaseFailed:
			connectErr = connect.NewError(connect.CodeInternal, err)
		case errdefs.ErrInternalServer:
			connectErr = connect.NewError(connect.CodeInternal, err)
		default:
			connectErr = connect.NewError(connect.CodeUnknown, err)
		}

		return nil, connectErr
	}

	var ranks []*guessesv1.RankElement
	for _, rank := range leaderboard.RankList {
		ranks = append(ranks, &guessesv1.RankElement{
			Score:  float32(rank.Score),
			UserId: rank.UserID,
		})
	}

	return connect.NewResponse(&guessesv1.GetLeaderboardResponse{
		Data: &guessesv1.LeaderboardData{
			Ranks: []*guessesv1.RankElement{},
		},
	}), nil
}

func (gs *GuessesServer) ResetScores(ctx context.Context, r *connect.Request[guessesv1.ResetScoresRequest]) (*connect.Response[guessesv1.ResetScoresResponse], error) {
	err := gs.usecase.ResetScores(ctx, r.Msg.RoomId, r.Msg.QuestionCount)
	if err != nil {
		var connectErr *connect.Error

		switch err {
		case errdefs.ErrDatabaseFailed:
			connectErr = connect.NewError(connect.CodeInternal, err)
		default:
			connectErr = connect.NewError(connect.CodeUnknown, err)
		}

		return nil, connectErr
	}

	return connect.NewResponse(&guessesv1.ResetScoresResponse{}), nil
}
