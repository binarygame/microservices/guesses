package models_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

func TestNewScore(t *testing.T) {
	s := models.NewScore(10)
	assert.NotNil(t, s)
	assert.Zero(t, s.CorrectAnswers)
	assert.Zero(t, s.WrongAnswers)
	assert.Zero(t, s.ComboMultiplierIdx)
	assert.Equal(t, uint32(10), s.TotalQuestions)
	assert.Zero(t, s.Pontuation)
}

func TestUpdateStatistics(t *testing.T) {
	t.Run("correct", func(t *testing.T) {
		s := models.Score{
			CorrectAnswers:     uint32(5),
			WrongAnswers:       uint32(2),
			ComboMultiplierIdx: uint32(1),
			TotalQuestions:     uint32(10),
			Pontuation:         100,
		}

		s.UpdateStatistics(true, 5000*time.Millisecond)

		assert.NotEmpty(t, s)
		assert.Equal(t, uint32(6), s.CorrectAnswers)
		assert.Equal(t, uint32(2), s.WrongAnswers)
		assert.Equal(t, uint32(2), s.ComboMultiplierIdx)
		assert.Equal(t, uint32(10), s.TotalQuestions)
		assert.Equal(t, 1557, int(s.Pontuation))
	})

	t.Run("wrong", func(t *testing.T) {
		s := models.Score{
			CorrectAnswers:     uint32(5),
			WrongAnswers:       uint32(2),
			ComboMultiplierIdx: uint32(1),
			TotalQuestions:     uint32(10),
			Pontuation:         100,
		}

		s.UpdateStatistics(false, 5000)

		assert.NotEmpty(t, s)
		assert.Equal(t, s.CorrectAnswers, uint32(5))
		assert.Equal(t, s.WrongAnswers, uint32(3))
		assert.Equal(t, s.ComboMultiplierIdx, uint32(0))
		assert.Equal(t, uint32(10), s.TotalQuestions)
		assert.Equal(t, s.Pontuation, float64(100))
	})
}
