package usecase

import (
	"context"
	"log/slog"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

func (gu *guesses) Subscribe(ctx context.Context, roomID string) (<-chan models.Event, error) {
	// New grouped logger
	logger := gu.logger.WithGroup("subscribe").With("roomID", roomID)

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "Subscribe called")

	room := gu.manager.GetOrCreateRoom(roomID)

	client := bingameutils.NewRoomManagerClient[models.Event]()
	room.AddClient(client)

	// Goroutine to handle context cancellation
	go func() {
		<-ctx.Done()
		room.RemoveClient(client.ClientID)
		close(client.Channel) // Close the client's channel when done
	}()

	return client.Channel, nil
}

func (gu *guesses) SubscribeToAll(ctx context.Context) (<-chan models.Event, error) {
	// New grouped logger
	logger := gu.logger.WithGroup("subscribe_to_all")

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "SubscribeToAll called")

	room := gu.manager.GetOrCreateBroadcastRoom()

	client := bingameutils.NewRoomManagerClient[models.Event]()
	room.AddClient(client)

	// Goroutine to handle context cancellation
	go func() {
		<-ctx.Done()
		room.RemoveClient(client.ClientID)
		close(client.Channel) // Close the client's channel when done
	}()

	return client.Channel, nil
}

// Start background tasks for the guesses usecase.
// This must be called at least once after creating the usecase.
func (gu *guesses) StartBackgroundTasks() {
	gu.repository.SubscribeToEvents(context.Background(), func(event models.Event) { gu.manager.Broadcast(event, event.GetRoomID()) })
}
