VERSION --arg-scope-and-set 0.7

FROM alpine:latest

build:
    COPY +generic-build/app .
    SAVE ARTIFACT app AS LOCAL build/app

generic-build:
    FROM golang:1.22-alpine
    WORKDIR /app_build

    ARG GOOS=$TARGETOS
    ARG GOARCH=$TARGETARCH
    ARG GOARM

    COPY go.mod go.sum ./
    RUN go mod download

    COPY --dir . ./
    RUN go build -o app cmd/guesses/main.go

    SAVE ARTIFACT ./app

build-image:
    ARG EARTHLY_GIT_SHORT_HASH
    ARG EARTHLY_GIT_BRANCH

    ARG registry=registry.gitlab.com/binarygame/microservices/guesses
    ARG branch
    ARG source=default
    ARG mergeid
    ARG tag

    LET tags = ""

    SET tags = "$EARTHLY_GIT_SHORT_HASH $tags"

    IF [ "$branch" != "" ]
        SET tags = "latest-$branch $tags"
    END
    
    IF [ "$branch" = "production" ]
        SET tags = "latest $tags"
    END

    IF [ "$tag" != "" ]
        SET tags = "$tag $tags"
    END

    IF [ "$source" = "merge_request_event" ]
        SET tags = "mr-$mergeid latest-mr-$mergeid $tags"
    END

    BUILD +build-amd64-image --tags "$tags" --registry "$registry"
    BUILD +build-arm64-image --tags "$tags" --registry "$registry"

build-amd64-image:
    ARG --required tags
    ARG --required registry

    FOR tag IN $tags
        FROM --platform=linux/amd64 amd64/alpine:latest
        COPY (+generic-build/app --GOARCH=amd64 --GOOS=linux) /app
        ENTRYPOINT ["/app"]
        SAVE IMAGE --push "$registry:$tag" "$registry:$tag-amd64"
    END

build-arm64-image:
    ARG --required tags
    ARG --required registry

    FOR tag IN $tags
        FROM --platform=linux/arm/v8 alpine:latest
        COPY --platform=linux/amd64 (+generic-build/app --GOARCH=arm64 --GOOS=linux) /app
        ENTRYPOINT ["/app"]
        SAVE IMAGE --push "$registry:$tag" "$registry:$tag-arm64"
    END
