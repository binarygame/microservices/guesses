package usecase

import (
	"context"
	"log/slog"
	"time"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/guesses/internal/repository"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
	roomsv1connect "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1/roomsv1connect"
)

type Guesses interface {
	Submit(ctx context.Context, roomID, userID, questionID string, correct bool, timeTaken time.Duration) error
	GetLeaderboard(ctx context.Context, roomID string) (*models.Leaderboard, error)
	ResetScores(ctx context.Context, roomID string, questionCount uint32) error
	RecoverUserState(ctx context.Context, roomID, userID string) (*[]models.Guess, error)

	Subscribe(ctx context.Context, roomId string) (<-chan models.Event, error)
	SubscribeToAll(ctx context.Context) (<-chan models.Event, error)
	StartBackgroundTasks()
}

type guesses struct {
	logger      *slog.Logger
	repository  repository.GuessesRepository
	roomsClient roomsv1connect.RoomsServiceClient
	telemetry   telemetry.TelemetryService
	manager     *bingameutils.RoomManager[models.Event]
}

func New(repo repository.GuessesRepository, logger *slog.Logger, telemetry telemetry.TelemetryService, roomsClient roomsv1connect.RoomsServiceClient) Guesses {
	guesses := &guesses{
		logger:      logger,
		repository:  repo,
		roomsClient: roomsClient,
		telemetry:   telemetry,
		manager:     bingameutils.NewRoomManager[models.Event](),
	}
	return guesses
}
