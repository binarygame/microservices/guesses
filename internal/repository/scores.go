package repository

import (
	"context"
	"strconv"

	"github.com/redis/go-redis/v9"
	"gitlab.com/binarygame/microservices/guesses/pkg/constants"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

func (gr *guessesRepository) GetUserScore(ctx context.Context, roomID, userID string) (*models.Score, error) {
	key := buildScoreKey(roomID, userID)

	var score models.Score

	res := gr.db.HGetAll(ctx, key)

	val, err := res.Result()
	if err != nil {
		return nil, err
	}
	if len(val) == 0 { // score not found
		return nil, nil // no error, just return nil
	}

	err = res.Scan(&score)
	if err != nil {
		return nil, err
	}

	return &score, nil
}

func (gr *guessesRepository) GetUserComboIdx(ctx context.Context, roomID, userID string) (uint32, error) {
	key := buildScoreKey(roomID, userID)

	res := gr.db.HGet(ctx, key, "combo")

	val, err := res.Result()
	if err != nil {
		return 0, err
	}
	if len(val) == 0 { // score not found
		return 0, nil // no error, just return nil
	}

	comboIdx, err := strconv.ParseUint(val, 10, 32)
	if err != nil {
		return 0, err
	}

	return uint32(comboIdx), nil
}

func (gr *guessesRepository) ResetRoomScores(ctx context.Context, roomId string, usersInRoom []string, questionCount uint32) error {
	lKey := buildLeaderboardKey(roomId)

	// Removes all elements in the sorted set stored at lKey
	err := gr.db.ZRemRangeByRank(ctx, lKey, 0, -1).Err()
	if err != nil {
		return err
	}

	err = gr.db.Expire(ctx, lKey, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	emptyScore := models.NewScore(questionCount)

	for _, userId := range usersInRoom {
		scKey := buildScoreKey(roomId, userId)

		_, err := gr.db.HSet(ctx, scKey, emptyScore).Result()
		if err != nil {
			return err
		}

		err = gr.db.ZAddNX(ctx, lKey, redis.Z{
			Score:  0,
			Member: userId,
		}).Err()
		if err != nil {
			return err
		}

		err = gr.db.Expire(ctx, scKey, constants.ExpireDuration).Err()
		if err != nil {
			return err
		}
	}

	return nil
}
