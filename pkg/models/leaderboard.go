package models

type Leaderboard struct {
	RankList []RankElement
}

type RankElement struct {
	UserID             string
	Score              float64
	ComboMultiplierIdx uint32
}
