package server

import (
	"log/slog"
	"net/http"
	"strconv"

	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/guesses/internal/usecase"

	"gitlab.com/binarygame/microservices/guesses/pkg/constants"
	guessesv1connect "gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1/guessesv1connect"

	"connectrpc.com/connect"
	"connectrpc.com/grpcreflect"
	"connectrpc.com/otelconnect"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

type GuessesServer struct {
	guessesv1connect.UnimplementedGuessesServiceHandler
	guessesv1connect.UnimplementedEventServiceHandler
	usecase usecase.Guesses
	logger  *slog.Logger
}

func newGuessesServer(usecase usecase.Guesses, logger *slog.Logger) *GuessesServer {
	return &GuessesServer{
		usecase: usecase,
		logger:  logger,
	}
}

func Start(ip string, port int, logger *slog.Logger, ts telemetry.TelemetryService, usecase usecase.Guesses) {
	addr := ip + ":" + strconv.Itoa(port)

	ts.NewCounter(constants.GuessesSubmittedCounterKey, "Number of guesses submitted", telemetry.IntCounterType)

	otelInterceptor, err := otelconnect.NewInterceptor(otelconnect.WithTrustRemote())
	if err != nil {
		logger.Error("An error occurred in otelconnect.NewInterceptor", "error", err)
	}

	server := newGuessesServer(usecase, logger)
	mux := http.NewServeMux()
	guessesPath, guessesHandler := guessesv1connect.NewGuessesServiceHandler(
		server,
		connect.WithInterceptors(otelInterceptor),
	)

	eventsPath, eventsHandler := guessesv1connect.NewEventServiceHandler(
		server,
		connect.WithInterceptors(otelInterceptor),
	)

	reflector := grpcreflect.NewStaticReflector(
		guessesv1connect.GuessesServiceName,
		guessesv1connect.EventServiceName,
	)

	mux.Handle(guessesPath, guessesHandler)
	mux.Handle(eventsPath, eventsHandler)

	mux.Handle(grpcreflect.NewHandlerV1(reflector))
	mux.Handle(grpcreflect.NewHandlerV1Alpha(reflector))

	logger.Info("Starting connect server for binarygame-guesses on " + addr)

	err = http.ListenAndServe(
		addr,
		h2c.NewHandler(mux, &http2.Server{}),
	)
	if err != nil {
		logger.Error("An error occurred in http.ListenAndServer: ", "error", err)
	}
}
