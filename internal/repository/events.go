package repository

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

// TODO: Publish only the event ID to Valkey and do not marshal the fields. Use Valkey Hash instead.

// PublishEvent publishes a model.Event to Valkey.
func (ru *guessesRepository) PublishEvent(ctx context.Context, event models.Event) error {
	eventJSON, err := json.Marshal(event)
	if err != nil {
		return fmt.Errorf("failed to marshal event: %w", err)
	}

	err = ru.db.Publish(ctx, eventsRedisKey, eventJSON).Err()
	if err != nil {
		return fmt.Errorf("failed to publish event to Valkey: %w", err)
	}

	return nil
}

// SubscribeToEvents subscribes to events from Valkey and converts them to model.Event.
func (gu *guessesRepository) SubscribeToEvents(ctx context.Context, eventHandler func(event models.Event)) error {
	pubsub := gu.db.Subscribe(ctx, eventsRedisKey)
	ch := pubsub.Channel()

	go func() {
		for msg := range ch {
			var baseEvent models.BaseEvent
			if err := json.Unmarshal([]byte(msg.Payload), &baseEvent); err != nil {
				fmt.Printf("failed to unmarshal event: %v\n", err)
				continue
			}

			event, err := models.NewEvent(baseEvent)
			if err != nil {
				fmt.Printf("failed to create event: %v\n", err)
				continue
			}

			if err := json.Unmarshal([]byte(msg.Payload), event); err != nil {
				fmt.Printf("failed to unmarshal event to specific type: %v\n", err)
				continue
			}

			eventHandler(event)
		}
	}()

	return nil
}
