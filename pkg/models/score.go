package models

import (
	"math"
	"time"

	"gitlab.com/binarygame/microservices/guesses/pkg/constants"
)

type Score struct {
	CorrectAnswers     uint32 `redis:"correct"`
	WrongAnswers       uint32 `redis:"wrong"`
	ComboMultiplierIdx uint32 `redis:"combo"`
	TotalQuestions     uint32 `redis:"total"`
	Pontuation         float64
}

func NewScore(totalQuestions uint32) *Score {
	return &Score{
		CorrectAnswers:     0,
		WrongAnswers:       0,
		ComboMultiplierIdx: 0,
		TotalQuestions:     totalQuestions,
		Pontuation:         0,
	}
}

// IncreasePontuation calculates the pontuation based on the time taken to answer the question
// and the combo multiplier value, following the formula:
// f(t) = BasePontuation * e^(DecayRate * t) * multiplier
func (s *Score) IncreasePontuation(timeTaken time.Duration, multiplier float64) {
	s.Pontuation = (constants.BasePontuation * math.Pow(math.E, -constants.DecayRate*float64(timeTaken.Milliseconds()))) * multiplier
}

func (s *Score) UpdateStatistics(isCorrect bool, timeTaken time.Duration) {
	if isCorrect {
		s.CorrectAnswers += 1
		s.ComboMultiplierIdx += 1 // if user get answer correct, increase the combo multiplier index
		s.IncreasePontuation(timeTaken, constants.MultiplierProgression[s.ComboMultiplierIdx])
	} else {
		s.WrongAnswers += 1
		s.ComboMultiplierIdx = 0 // otherwise reset multiplier
	}
}
