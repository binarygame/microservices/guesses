package repository

const (
	guessesRedisKey      = "guesses"
	usersRedisKey        = "users"
	roomsRedisKey        = "rooms"
	scoreRedisKey        = "score"
	questionsRedisKey    = "questions"
	leaderboardsRedisKey = "leaderboard"
	eventsRedisKey       = "events:guesses"
)

func buildRoomGuessesKey(roomID string) string {
	return roomsRedisKey + ":" + roomID + ":" + guessesRedisKey
}

func buildUserGuessKey(userID, questionID string) string {
	return usersRedisKey + ":" + userID + ":" + questionsRedisKey + ":" + questionID
}

func buildScoreKey(roomID, userID string) string {
	return roomsRedisKey + ":" + roomID + ":" + usersRedisKey + ":" + userID + ":" + scoreRedisKey
}

func buildRoomScoreKey(roomID string) string {
	return roomsRedisKey + ":" + roomID + ":" + scoreRedisKey
}

func buildLeaderboardKey(roomId string) string {
	return roomsRedisKey + ":" + roomId + ":" + leaderboardsRedisKey
}

func buildUserRoomSetKey(roomId string) string {
	return roomsRedisKey + ":" + roomId + ":" + usersRedisKey
}

func buildUserKey(userID string) string {
	return usersRedisKey + ":" + userID
}
