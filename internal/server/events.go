package server

import (
	"context"

	"connectrpc.com/connect"
	guessesv1 "gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (rs *GuessesServer) SubscribeAllGuessesEvents(ctx context.Context, r *connect.Request[guessesv1.SubscribeAllGuessesEventsRequest], s *connect.ServerStream[guessesv1.SubscribeAllGuessesEventsResponse]) error {
	// Subscribe to room events
	rs.logger.Info("Subscribing to all guesses events")
	ch, err := rs.usecase.SubscribeToAll(ctx)
	if err != nil {
		rs.logger.Error("Failed to subscribe to all guesses events", "error", err)
		return connect.NewError(connect.CodeInternal, err)
	}

	for {
		select {
		case <-ctx.Done():
			rs.logger.Info("Context done, terminating subscription to all guesses")
			return nil
		case event := <-ch:
			protoEvent := convertEvent(event)

			rs.logger.Debug("Sending event",
				"roomId", event.GetRoomID(),
				"timestamp", event.GetTimestamp().String(),
				"description", event.GetDescription(),
				"eventType", protoEvent.EventType.String(),
			)

			// Send the event via the stream
			err = s.Send(&guessesv1.SubscribeAllGuessesEventsResponse{
				Data: protoEvent,
			})
			if err != nil {
				rs.logger.Error("Failed to send event via stream", "error", err)
				return connect.NewError(connect.CodeInternal, err)
			}
		}
	}
}

func (rs *GuessesServer) SubscribeGuessesEvents(ctx context.Context, r *connect.Request[guessesv1.SubscribeGuessesEventsRequest], s *connect.ServerStream[guessesv1.SubscribeGuessesEventsResponse]) error {
	// Subscribe to room events
	rs.logger.Info("Subscribing to room events", "roomId", r.Msg.GetRoomId())
	ch, err := rs.usecase.Subscribe(ctx, r.Msg.GetRoomId())
	if err != nil {
		rs.logger.Error("Failed to subscribe to room events", "error", err)
		return connect.NewError(connect.CodeInternal, err)
	}

	for {
		select {
		case <-ctx.Done():
			rs.logger.Info("Context done, terminating subscription", "roomId", r.Msg.GetRoomId())
			return nil
		case event := <-ch:
			protoEvent := convertEvent(event)

			rs.logger.Debug("Sending event",
				"roomId", event.GetRoomID(),
				"timestamp", event.GetTimestamp().String(),
				"description", event.GetDescription(),
				"eventType", protoEvent.EventType.String(),
			)

			// Send the event via the stream
			err = s.Send(&guessesv1.SubscribeGuessesEventsResponse{
				Data: protoEvent,
			})
			if err != nil {
				rs.logger.Error("Failed to send event via stream", "error", err)
				return connect.NewError(connect.CodeInternal, err)
			}
		}
	}
}

func convertEvent(event models.Event) *guessesv1.Event {
	protoEvent := &guessesv1.Event{
		EventId:   event.GetID(),
		EventTime: timestamppb.New(event.GetTimestamp()),
		RoomId:    event.GetRoomID(),
	}

	// Optional description
	if d := event.GetDescription(); d != "" {
		protoEvent.Description = &d
	}

	switch e := event.(type) {
	case *models.LeaderboardUpdatedEvent:
		protoLdbUpdate := &guessesv1.LeaderboardUpdatedEvent{
			Data: convertLeaderboardToProtoFmt(e.GetLeaderboard()),
		}
		protoEvent.EventType = guessesv1.EventType_EVENT_TYPE_LEADERBOARD_UPDATED
		protoEvent.Event = &guessesv1.Event_LeaderboardUpdatedEvent{LeaderboardUpdatedEvent: protoLdbUpdate}
	case *models.GenericEvent:
		protoGenericEvent := &guessesv1.GenericEvent{}
		switch event.GetType() {
		case models.GuessSubmitted:
			protoEvent.EventType = guessesv1.EventType_EVENT_TYPE_GUESS_SUBMITTED
		}
		protoEvent.Event = &guessesv1.Event_GenericEvent{GenericEvent: protoGenericEvent}
	default:
		protoEvent.EventType = guessesv1.EventType_EVENT_TYPE_UNSPECIFIED
	}

	return protoEvent
}

func convertLeaderboardToProtoFmt(curr *models.Leaderboard) *guessesv1.LeaderboardData {
	ranks := make([]*guessesv1.RankElement, 0, len(curr.RankList))

	for _, rank := range curr.RankList {
		ranks = append(ranks, &guessesv1.RankElement{
			UserId:             rank.UserID,
			Score:              float32(rank.Score),
			ComboMultiplierIdx: rank.ComboMultiplierIdx,
		})
	}

	return &guessesv1.LeaderboardData{Ranks: ranks}
}
