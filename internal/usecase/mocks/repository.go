package mocks

import (
	"context"

	"github.com/stretchr/testify/mock"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

type MockGuessesRepository struct {
	mock.Mock
}

func (m *MockGuessesRepository) StoreGuess(ctx context.Context, guess *models.Guess, score *models.Score) error {
	args := m.Called(ctx, guess, score)
	return args.Error(0)
}

func (m *MockGuessesRepository) UserAlreadyAnswered(ctx context.Context, guess *models.Guess) (bool, error) {
	args := m.Called(ctx, guess)
	return args.Bool(0), args.Error(1)
}

func (m *MockGuessesRepository) GetUserScore(ctx context.Context, roomID, userID string) (*models.Score, error) {
	args := m.Called(ctx, roomID, userID)

	if args.Get(0) != nil {
		return args.Get(0).(*models.Score), args.Error(1)
	}

	return nil, args.Error(1)
}

func (m *MockGuessesRepository) GetUserComboIdx(ctx context.Context, roomID, userID string) (uint32, error) {
	args := m.Called(ctx, roomID, userID)

	if args.Get(0) != nil {
		return args.Get(0).(uint32), args.Error(1)
	}

	return 0, args.Error(1)
}

func (m *MockGuessesRepository) ResetRoomScores(ctx context.Context, roomID string, usersInRoom []string, questionCount uint32) error {
	args := m.Called(ctx, roomID, usersInRoom, questionCount)
	return args.Error(0)
}

func (m *MockGuessesRepository) GetLeaderboard(ctx context.Context, roomID string) (*models.Leaderboard, error) {
	args := m.Called(ctx, roomID)
	if args.Get(0) != nil {
		return args.Get(0).(*models.Leaderboard), args.Error(1)
	}

	return nil, args.Error(1)
}

func (m *MockGuessesRepository) PublishEvent(ctx context.Context, event models.Event) error {
	args := m.Called(ctx, event)
	return args.Error(0)
}

func (m *MockGuessesRepository) SubscribeToEvents(ctx context.Context, eventHandler func(event models.Event)) error {
	args := m.Called(ctx, eventHandler)
	return args.Error(0)
}
