package constants

import "time"

// app config
const (
	ExpireDuration         = 2 * time.Hour
	BasePontuation float64 = 1000
	DecayRate      float64 = 0.00005
)

var MultiplierProgression = [5]float64{1.0, 1.5, 2.0, 2.5, 3.0}

// telemetry config
const (
	NamespaceKey               = "gitlab.com/binarygame/microservices/guesses"
	GuessesSubmittedCounterKey = "guesses_submitted"
)
