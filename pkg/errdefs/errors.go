package errdefs

import (
	"errors"
)

var (
	ErrInvalidData         = errors.New("data provided is invalid")
	ErrUserAlreadyAnswered = errors.New("overwriting answer is not allowed")
	ErrDatabaseFailed      = errors.New("an error occurred while connecting with Redis")
	ErrInternalServer      = errors.New("internal server error")

	/*
	   Event specific errors
	*/
	ErrUlidInvalid      = errors.New("the event ULID is invalid")
	ErrEventTypeInvalid = errors.New("unknown event type")
)
