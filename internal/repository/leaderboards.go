package repository

import (
	"context"

	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

func (gr *guessesRepository) GetLeaderboard(ctx context.Context, roomID string) (*models.Leaderboard, error) {
	lKey := buildLeaderboardKey(roomID)

	scrList, err := gr.db.ZRevRangeWithScores(ctx, lKey, 0, -1).Result() // returns all elements from the sorted set
	if err != nil {
		return nil, err
	}

	rankList := make([]models.RankElement, len(scrList))

	for i, item := range scrList {
		userID := item.Member.(string)
		userComboIdx, err := gr.GetUserComboIdx(ctx, roomID, userID)
		if err != nil {
			return nil, err
		}
		rankList[i] = models.RankElement{
			UserID:             userID,
			Score:              item.Score,
			ComboMultiplierIdx: userComboIdx,
		}
	}

	return &models.Leaderboard{RankList: rankList}, nil
}
