package repository

import (
	"context"

	"gitlab.com/binarygame/microservices/guesses/pkg/constants"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

func (gr *guessesRepository) StoreGuess(ctx context.Context, guess *models.Guess, score *models.Score) error {
	rgKey := buildRoomGuessesKey(guess.RoomID)
	ugKey := buildUserGuessKey(guess.UserID, guess.QuestionID)
	scKey := buildScoreKey(guess.RoomID, guess.UserID)
	lKey := buildLeaderboardKey(guess.RoomID)

	pipe := gr.db.Pipeline()

	pipe.SAdd(ctx, rgKey, ugKey) // SADD "rooms:xyz:guesses" "users:abc:questions:123"
	pipe.Expire(ctx, rgKey, constants.ExpireDuration)

	pipe.HSet(ctx, scKey, score) // HSET "rooms:xyz:users:abc:score" correct 2 ...
	pipe.Expire(ctx, scKey, constants.ExpireDuration)

	pipe.ZIncrBy(ctx, lKey, score.Pontuation, guess.UserID) // ZINCRBY "rooms:xyz:leaderboard" 150 abc

	_, err := pipe.Exec(ctx)
	if err != nil {
		gr.logger.Error("Error executing pipeline", "error", err)
		return err
	}

	return nil
}

func (gr *guessesRepository) UserAlreadyAnswered(ctx context.Context, guess *models.Guess) (bool, error) {
	rgkey := buildRoomGuessesKey(guess.RoomID)
	ugKey := buildUserGuessKey(guess.UserID, guess.QuestionID)

	return gr.db.SIsMember(ctx, rgkey, ugKey).Result() // SISMEMBER "rooms:xyz:guesses" "users:abc:questions:123"
}
