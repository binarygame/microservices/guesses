package server

import (
	"bytes"
	"context"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"connectrpc.com/connect"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/binarygame/microservices/guesses/internal/server/mocks"
	guessesv1 "gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1"
	guessesv1connect "gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1/guessesv1connect"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
	"google.golang.org/protobuf/types/known/durationpb"
)

var (
	logOutput bytes.Buffer
	logger    = slog.New(slog.NewTextHandler(&logOutput, nil))
)

func initTestServer() (*httptest.Server, *mocks.MockGuessesUsecase) {
	mockUsecase := new(mocks.MockGuessesUsecase)
	service := newGuessesServer(mockUsecase, logger)

	mux := http.NewServeMux()
	mux.Handle(guessesv1connect.NewGuessesServiceHandler(
		service,
	))

	testServer := httptest.NewUnstartedServer(mux)
	testServer.EnableHTTP2 = true
	return testServer, mockUsecase
}

func initClients(testServer *httptest.Server) []guessesv1connect.GuessesServiceClient {
	connectClient := guessesv1connect.NewGuessesServiceClient(
		testServer.Client(),
		testServer.URL,
	)
	connectGrpcClient := guessesv1connect.NewGuessesServiceClient(
		testServer.Client(),
		testServer.URL,
		connect.WithGRPC(),
	)
	return []guessesv1connect.GuessesServiceClient{connectClient, connectGrpcClient}
}

func TestSubmitGuess(t *testing.T) {
	t.Parallel()

	t.Run("success", func(t *testing.T) {
		testServer, mockUsecase := initTestServer()
		testServer.StartTLS()
		defer testServer.Close()

		clients := initClients(testServer)

		for _, client := range clients {
			mockUsecase.On("Submit", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			result, err := client.SubmitGuess(
				context.Background(),
				connect.NewRequest(&guessesv1.SubmitGuessRequest{
					UserId:     "1234",
					RoomId:     "abcd",
					QuestionId: "efgh",
					IsCorrect:  true,
					TimeTaken:  durationpb.New(5000 * time.Millisecond),
				}))
			require.Nil(t, err)
			require.NotNil(t, result)
		}
		mockUsecase.AssertExpectations(t)
	})
	t.Run("invalid input", func(t *testing.T) {
		testServer, mockUsecase := initTestServer()
		testServer.StartTLS()
		defer testServer.Close()

		clients := initClients(testServer)

		for _, client := range clients {
			mockUsecase.On("Submit", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(assert.AnError).Once()
			result, err := client.SubmitGuess(
				context.Background(),
				connect.NewRequest(&guessesv1.SubmitGuessRequest{
					UserId:     "", // Invalid user ID
					RoomId:     "abcd",
					QuestionId: "efgh",
					IsCorrect:  true,
					TimeTaken:  durationpb.New(5000 * time.Millisecond),
				}))
			require.NotNil(t, err)
			require.Nil(t, result)
		}
		mockUsecase.AssertExpectations(t)
	})
}

func TestGetLeaderboard(t *testing.T) {
	t.Parallel()

	t.Run("success", func(t *testing.T) {
		testServer, mockUsecase := initTestServer()
		testServer.StartTLS()
		defer testServer.Close()

		clients := initClients(testServer)

		for _, client := range clients {
			mockUsecase.On("GetLeaderboard", mock.Anything, mock.Anything).Return(&models.Leaderboard{}, nil).Once()
			result, err := client.GetLeaderboard(
				context.Background(),
				connect.NewRequest(&guessesv1.GetLeaderboardRequest{
					RoomId: "abcd",
				}))
			require.Nil(t, err)
			require.NotNil(t, result)
		}
		mockUsecase.AssertExpectations(t)
	})

	t.Run("fail", func(t *testing.T) {
		testServer, mockUsecase := initTestServer()
		testServer.StartTLS()
		defer testServer.Close()

		clients := initClients(testServer)

		for _, client := range clients {
			mockUsecase.On("GetLeaderboard", mock.Anything, mock.Anything).Return(nil, assert.AnError).Once()
			result, err := client.GetLeaderboard(
				context.Background(),
				connect.NewRequest(&guessesv1.GetLeaderboardRequest{
					RoomId: "", // Invalid room ID
				}))
			require.NotNil(t, err)
			require.Nil(t, result)
		}
		mockUsecase.AssertExpectations(t)
	})
}

func TestResetScores(t *testing.T) {
	t.Parallel()

	t.Run("success", func(t *testing.T) {
		testServer, mockUsecase := initTestServer()
		testServer.StartTLS()
		defer testServer.Close()

		clients := initClients(testServer)

		for _, client := range clients {
			mockUsecase.On("ResetScores", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			result, err := client.ResetScores(
				context.Background(),
				connect.NewRequest(&guessesv1.ResetScoresRequest{
					RoomId: "abcd",
				}))
			require.Nil(t, err)
			require.NotNil(t, result)
		}
		mockUsecase.AssertExpectations(t)
	})

	t.Run("fail", func(t *testing.T) {
		testServer, mockUsecase := initTestServer()
		testServer.StartTLS()
		defer testServer.Close()

		clients := initClients(testServer)

		for _, client := range clients {
			mockUsecase.On("ResetScores", mock.Anything, mock.Anything, mock.Anything).Return(assert.AnError).Once()
			result, err := client.ResetScores(
				context.Background(),
				connect.NewRequest(&guessesv1.ResetScoresRequest{
					RoomId: "",
				}))
			require.NotNil(t, err)
			require.Nil(t, result)
		}
		mockUsecase.AssertExpectations(t)
	})
}
