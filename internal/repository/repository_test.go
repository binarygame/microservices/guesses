package repository_test

import (
	"bytes"
	"context"
	"log/slog"
	"testing"

	"github.com/alicebob/miniredis/v2"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/require"
	"gitlab.com/binarygame/microservices/guesses/internal/repository"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

func setupTestRedis(t *testing.T) (*redis.Client, *miniredis.Miniredis) {
	s := miniredis.RunT(t)

	client := redis.NewClient(&redis.Options{
		Addr: s.Addr(),
	})

	return client, s
}

var (
	loggerOutput bytes.Buffer
	logger       = slog.New(slog.NewTextHandler(&loggerOutput, nil))
)

func TestStoreGuess(t *testing.T) {
	guess1Obj := &models.Guess{RoomID: "xyz", UserID: "abc", QuestionID: "123", Correct: true}
	guess2Obj := &models.Guess{RoomID: "xyz", UserID: "iop", QuestionID: "456", Correct: false}
	score1Obj := &models.Score{CorrectAnswers: 4, WrongAnswers: 3, ComboMultiplierIdx: 2}
	score2Obj := &models.Score{CorrectAnswers: 4, WrongAnswers: 4, ComboMultiplierIdx: 0}

	t.Run("success", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		err := repo.StoreGuess(ctx, guess1Obj, score1Obj)
		require.NoError(t, err)

		err = repo.StoreGuess(ctx, guess2Obj, score2Obj)
		require.NoError(t, err)
	})

	t.Run("fail", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Simulate Redis failure
		s.Close()

		err := repo.StoreGuess(ctx, guess1Obj, score1Obj)
		require.Error(t, err)
	})
}

func TestUserAlreadyAnswered(t *testing.T) {
	guessObj := models.Guess{RoomID: "xyz", UserID: "abc", QuestionID: "123", Correct: true}

	t.Run("true", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Store guess first
		err := repo.StoreGuess(ctx, &guessObj, &models.Score{})
		require.NoError(t, err)

		// Check if user already answered
		result, err := repo.UserAlreadyAnswered(ctx, &guessObj)
		require.NoError(t, err)
		require.True(t, result)
	})

	t.Run("false", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Check if user already answered
		result, err := repo.UserAlreadyAnswered(ctx, &guessObj)
		require.NoError(t, err)
		require.False(t, result)
	})

	t.Run("fail", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Simulate Redis failure
		s.Close()

		_, err := repo.UserAlreadyAnswered(ctx, &guessObj)
		require.Error(t, err)
	})
}

func TestGetUserScore(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Assume we have a score stored for the user
		scoreObj := models.Score{CorrectAnswers: 4, WrongAnswers: 3, ComboMultiplierIdx: 2}
		err := repo.StoreGuess(ctx, &models.Guess{RoomID: "xyz", UserID: "abc", QuestionID: "123", Correct: true}, &scoreObj)
		require.NoError(t, err)

		// Retrieve user score
		score, err := repo.GetUserScore(ctx, "xyz", "abc")
		require.NoError(t, err)
		require.Equal(t, &scoreObj, score)
	})

	t.Run("inexistent data", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Attempt to get score for non-existent data
		score, err := repo.GetUserScore(ctx, "xyz", "def")
		require.NoError(t, err)
		require.Nil(t, score)
	})

	t.Run("fail", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Simulate Redis failure
		s.Close()

		_, err := repo.GetUserScore(ctx, "xyz", "abc")
		require.Error(t, err)
	})
}

func TestResetRoomScores(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Assume we have scores stored for the room
		err := repo.StoreGuess(ctx, &models.Guess{RoomID: "xyz", UserID: "abc", QuestionID: "123", Correct: true}, &models.Score{})
		require.NoError(t, err)

		// Reset room scores
		err = repo.ResetRoomScores(ctx, "xyz", []string{"a1", "b2"}, 10)
		require.NoError(t, err)

		// Check that scores are reset
		score, err := repo.GetUserScore(ctx, "xyz", "abc")
		require.NoError(t, err)
		require.NotNil(t, score)
	})

	t.Run("fail", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Simulate Redis failure
		s.Close()

		err := repo.ResetRoomScores(ctx, "xyz", []string{"a1", "b2"}, 10)
		require.Error(t, err)
	})
}

func TestGetLeaderboard(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Assume we have scores stored for the room
		err := repo.StoreGuess(ctx, &models.Guess{RoomID: "xyz", UserID: "abc", QuestionID: "123", Correct: true}, &models.Score{CorrectAnswers: 4, WrongAnswers: 3, ComboMultiplierIdx: 2})
		require.NoError(t, err)
		err = repo.StoreGuess(ctx, &models.Guess{RoomID: "xyz", UserID: "def", QuestionID: "456", Correct: true}, &models.Score{CorrectAnswers: 2, WrongAnswers: 1, ComboMultiplierIdx: 1})
		require.NoError(t, err)

		// Retrieve leaderboard
		leaderboard, err := repo.GetLeaderboard(ctx, "xyz")
		require.NoError(t, err)
		require.NotNil(t, leaderboard)
	})

	t.Run("empty", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Retrieve leaderboard for empty room
		leaderboard, err := repo.GetLeaderboard(ctx, "xyz")
		require.NoError(t, err)
		require.NotNil(t, leaderboard)
		require.Equal(t, 0, len(leaderboard.RankList))
	})

	t.Run("fail", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Simulate Redis failure
		s.Close()

		_, err := repo.GetLeaderboard(ctx, "xyz")
		require.Error(t, err)
	})
}

func TestPublishEvent(t *testing.T) {
	eventObj := models.NewGenericEvent(1, "xyz", "description")

	t.Run("success", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		err := repo.PublishEvent(ctx, eventObj)
		require.NoError(t, err)
	})

	t.Run("fail", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr, logger)
		ctx := context.Background()

		// Simulate Redis failure
		s.Close()

		err := repo.PublishEvent(ctx, eventObj)
		require.Error(t, err)
	})
}
