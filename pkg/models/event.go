package models

import (
	"strconv"
	"time"

	"github.com/oklog/ulid/v2"
	"gitlab.com/binarygame/microservices/guesses/pkg/errdefs"
)

type EventType int

const (
	/* events */
	GuessSubmitted EventType = iota
	LeaderboardUpdated

	/* errors */
)

func (e EventType) String() string {
	switch e {
	case GuessSubmitted:
		return "GuessSubmitted"
	case LeaderboardUpdated:
		return "LeaderboardUpdated"
	default:
		return "Unknown:" + strconv.Itoa(int(e))
	}
}

// Event interface definition
type Event interface {
	GetID() string
	GetTimestamp() time.Time
	GetType() EventType
	GetDescription() string
	GetRoomID() string
}

// BaseEvent struct with embedded fields
type BaseEvent struct {
	ID          ulid.ULID
	Timestamp   time.Time
	Type        EventType
	RoomID      string
	Description string
}

type GenericEvent struct {
	BaseEvent
}

type GuessEvent struct {
	BaseEvent
	UserId     string
	QuestionId string
	Correct    bool
}

type LeaderboardUpdatedEvent struct {
	BaseEvent
	Leaderboard *Leaderboard
}

/*
  Implement the Event interface for the base type
*/

func (e *BaseEvent) GetID() string {
	return e.ID.String()
}

func (e *BaseEvent) GetTimestamp() time.Time {
	return e.Timestamp
}

func (e *BaseEvent) GetType() EventType {
	return e.Type
}

func (e *BaseEvent) GetDescription() string {
	return e.Description
}

func (e *BaseEvent) GetRoomID() string {
	return e.RoomID
}

/*
  Leaderboard event functions
*/

func (e *LeaderboardUpdatedEvent) GetLeaderboard() *Leaderboard {
	return e.Leaderboard
}

/*
  Implement the Event interface methods for GenericEvent
*/

// Factory function for creating a new GenericEvent
func NewGenericEvent(eventType EventType, roomId string, description string) Event {
	return &GenericEvent{
		BaseEvent: BaseEvent{
			ID:          ulid.Make(),
			Timestamp:   time.Now(),
			Type:        eventType,
			Description: description,
			RoomID:      roomId,
		},
	}
}

// Factory function for creating a new GenericEvent.
// This function is useful for synchronizing the event with other functions.
func NewGenericEventWithTimestamp(timestamp time.Time, eventType EventType, roomId string, description string) Event {
	return &GenericEvent{
		BaseEvent: BaseEvent{
			ID:          ulid.Make(),
			Timestamp:   timestamp,
			Type:        eventType,
			RoomID:      roomId,
			Description: description,
		},
	}
}

func NewGuessEvent(eventType EventType, roomId, userId, questionId string, correct bool, description string) Event {
	return &GuessEvent{
		BaseEvent: BaseEvent{
			ID:          ulid.Make(),
			Timestamp:   time.Now(),
			Type:        eventType,
			Description: description,
			RoomID:      roomId,
		},
		UserId:     userId,
		QuestionId: questionId,
		Correct:    correct,
	}
}

func NewLeaderboardUpdatedEvent(eventType EventType, roomId string, leaderboard *Leaderboard, description string) Event {
	return &LeaderboardUpdatedEvent{
		BaseEvent: BaseEvent{
			ID:          ulid.Make(),
			Timestamp:   time.Now(),
			Type:        eventType,
			Description: description,
			RoomID:      roomId,
		},
		Leaderboard: leaderboard,
	}
}

// NewEvent creates a specific Event based on the BaseEvent Type
func NewEvent(baseEvent BaseEvent) (Event, error) {
	switch baseEvent.Type {
	case GuessSubmitted:
		return &GuessEvent{BaseEvent: baseEvent}, nil
	case LeaderboardUpdated:
		return &LeaderboardUpdatedEvent{BaseEvent: baseEvent}, nil
	default:
		return nil, errdefs.ErrEventTypeInvalid
	}
}
