package repository

import (
	"context"
	"log/slog"

	"gitlab.com/binarygame/microservices/guesses/pkg/models"

	"github.com/redis/go-redis/extra/redisotel/v9"
	"github.com/redis/go-redis/v9"
)

type GuessesRepository interface {
	/*
	   events.go functions
	*/
	PublishEvent(ctx context.Context, event models.Event) error
	SubscribeToEvents(ctx context.Context, eventHandler func(event models.Event)) error

	/*
		guesses.go functions
	*/
	StoreGuess(ctx context.Context, guess *models.Guess, score *models.Score) error
	UserAlreadyAnswered(ctx context.Context, guess *models.Guess) (bool, error)

	/*
		scores.go functions
	*/
	GetUserScore(ctx context.Context, roomID, userID string) (*models.Score, error)
	GetUserComboIdx(ctx context.Context, roomID, userID string) (uint32, error)
	ResetRoomScores(ctx context.Context, roomId string, usersInRoom []string, questionCount uint32) error

	/*
		leaderboards.go functions
	*/
	GetLeaderboard(ctx context.Context, roomID string) (*models.Leaderboard, error)
}

type guessesRepository struct {
	db     *redis.Client
	logger *slog.Logger
}

func New(address string, logger *slog.Logger) *guessesRepository {
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	// Enable tracing instrumentation.
	if err := redisotel.InstrumentTracing(client); err != nil {
		logger.Error("Could not enable redisotel tracing instrumentation", "error", err)
	}

	// Enable metrics instrumentation.
	if err := redisotel.InstrumentMetrics(client); err != nil {
		logger.Error("Could not enable redisotel metrics instrumentation", "error", err)
	}

	return &guessesRepository{
		db:     client,
		logger: logger,
	}
}
