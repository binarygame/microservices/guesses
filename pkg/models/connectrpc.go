package models

import (
	"github.com/oklog/ulid/v2"
	"gitlab.com/binarygame/microservices/guesses/pkg/errdefs"
	guessesv1 "gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1"
)

type eventCreator func(*guessesv1.Event, BaseEvent) (Event, error)

var eventTypeHandlers = map[guessesv1.EventType]eventCreator{
	guessesv1.EventType_EVENT_TYPE_GUESS_SUBMITTED:     createGuessEvent,
	guessesv1.EventType_EVENT_TYPE_LEADERBOARD_UPDATED: createLeaderboardUpdatedEvent,
}

func createLeaderboardUpdatedEvent(protoEvent *guessesv1.Event, baseEvent BaseEvent) (Event, error) {
	leaderboardUpdatedEvent := protoEvent.GetLeaderboardUpdatedEvent()
	if leaderboardUpdatedEvent == nil {
		return nil, errdefs.ErrEventTypeInvalid
	}

	// Convert leaderboard
	rankElements := make([]RankElement, len(leaderboardUpdatedEvent.Data.Ranks))
	for i, rankElement := range leaderboardUpdatedEvent.Data.Ranks {
		rankElements[i] = RankElement{
			UserID:             rankElement.UserId,
			Score:              float64(rankElement.Score), // Maybe change Score to float32 later, or maybe event int
			ComboMultiplierIdx: rankElement.ComboMultiplierIdx,
		}
	}

	baseEvent.Type = LeaderboardUpdated
	return &LeaderboardUpdatedEvent{
		BaseEvent: baseEvent,
		Leaderboard: &Leaderboard{
			RankList: rankElements,
		},
	}, nil
}

func createGuessEvent(protoEvent *guessesv1.Event, baseEvent BaseEvent) (Event, error) {
	guessEvent := protoEvent.GetGuessEvent()
	if guessEvent == nil {
		return nil, errdefs.ErrEventTypeInvalid
	}
	baseEvent.Type = GuessSubmitted
	return &GuessEvent{
		BaseEvent:  baseEvent,
		UserId:     guessEvent.UserId,
		QuestionId: guessEvent.QuestionId,
		Correct:    guessEvent.Correct,
	}, nil
}

func createGenericEvent(eventType EventType) eventCreator {
	return func(protoEvent *guessesv1.Event, baseEvent BaseEvent) (Event, error) {
		baseEvent.Type = eventType
		return &GenericEvent{BaseEvent: baseEvent}, nil
	}
}

// ConvertProtoEvent converts the room event type from the ConnectRPC format back to the
// internal event format.
//
// Returns ErrEventTypeInvalid if the event is malformed.
//
// Returns ErrUlidInvalid if the event id can't be parsed as an ULID.
func ConvertProtoEvent(protoEvent *guessesv1.Event) (Event, error) {
	// Convert id
	id, err := ulid.ParseStrict(protoEvent.EventId)
	if err != nil {
		return nil, errdefs.ErrUlidInvalid
	}

	// Base event initialization
	baseEvent := BaseEvent{
		Type:        GuessSubmitted, // Default type to ensure it's set
		ID:          id,
		Timestamp:   protoEvent.EventTime.AsTime(),
		RoomID:      protoEvent.GetRoomId(),
		Description: protoEvent.GetDescription(),
	}

	// Use the map to find the appropriate event creator function
	if handler, exists := eventTypeHandlers[protoEvent.EventType]; exists {
		return handler(protoEvent, baseEvent)
	}
	return nil, errdefs.ErrEventTypeInvalid
}
