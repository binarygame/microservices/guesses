package main

import (
	"context"
	"errors"
	"log/slog"
	"net/http"

	"github.com/joho/godotenv"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/guesses/internal/repository"
	service "gitlab.com/binarygame/microservices/guesses/internal/server"
	"gitlab.com/binarygame/microservices/guesses/internal/usecase"
	"gitlab.com/binarygame/microservices/guesses/pkg/constants"
	"gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1/roomsv1connect"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
)

func main() {
	logger := bingameutils.GetLogger()
	slog.SetDefault(logger)

	err := godotenv.Load()
	if err != nil {
		slog.Info("Error loading .env file")
	}

	serviceHost := bingameutils.GetServiceHost()
	servicePort := bingameutils.GetServicePort()

	// Set up OpenTelemetry.
	ts, otelShutdown, err := telemetry.New(context.Background(), constants.NamespaceKey)
	if err != nil {
		slog.Error("failed to set up OpenTelemetry SDK", "error", err)
	}
	defer func() {
		err = errors.Join(err, otelShutdown(context.Background()))
	}()

	roomsClient := roomsv1connect.NewRoomsServiceClient(
		http.DefaultClient,
		"http://"+bingameutils.GetRoomsServiceAddress(),
	)

	repository := repository.New(bingameutils.GetValkeyAddress(), logger)
	usecase := usecase.New(repository, logger, ts, roomsClient)
	usecase.StartBackgroundTasks()

	service.Start(serviceHost, servicePort, logger, ts, usecase)
}
