package models_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

func TestNewGuess(t *testing.T) {
	validRID := "1234"
	validUID := uuid.NewString()
	validQID := uuid.NewString()

	t.Run("success", func(t *testing.T) {
		g, err := models.NewGuess(validRID, validUID, validQID, true)
		require.NoError(t, err)
		assert.NotNil(t, g)
		assert.Equal(t, validUID, g.UserID)
		assert.Equal(t, validQID, g.QuestionID)
		assert.Equal(t, validRID, g.RoomID)
		assert.True(t, g.Correct)
	})

	t.Run("invalid data", func(t *testing.T) {
		g, err := models.NewGuess("abc", validUID, validQID, false)
		assert.Nil(t, g)
		assert.Error(t, err)

		g, err = models.NewGuess(validRID, "abc", validQID, false)
		assert.Nil(t, g)
		assert.Error(t, err)

		g, err = models.NewGuess(validRID, validUID, "", false)
		assert.Nil(t, g)
		assert.Error(t, err)
	})
}
