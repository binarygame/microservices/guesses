package mocks

import (
	"context"

	"connectrpc.com/connect"
	"github.com/stretchr/testify/mock"
	v1 "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1"
)

type MockRoomsServiceClient struct {
	mock.Mock
}

func (m *MockRoomsServiceClient) CreateRoom(context.Context, *connect.Request[v1.CreateRoomRequest]) (*connect.Response[v1.CreateRoomResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.CreateRoomResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) RegisterUserInRoom(context.Context, *connect.Request[v1.RegisterUserInRoomRequest]) (*connect.Response[v1.RegisterUserInRoomResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.RegisterUserInRoomResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) GetBannedUsersFromRoom(context.Context, *connect.Request[v1.GetBannedUsersFromRoomRequest]) (*connect.Response[v1.GetBannedUsersFromRoomResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.GetBannedUsersFromRoomResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) RemoveUserFromRoom(context.Context, *connect.Request[v1.RemoveUserFromRoomRequest]) (*connect.Response[v1.RemoveUserFromRoomResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.RemoveUserFromRoomResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) BanUserFromRoom(context.Context, *connect.Request[v1.BanUserFromRoomRequest]) (*connect.Response[v1.BanUserFromRoomResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.BanUserFromRoomResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) UnbanUserFromRoom(context.Context, *connect.Request[v1.UnbanUserFromRoomRequest]) (*connect.Response[v1.UnbanUserFromRoomResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.UnbanUserFromRoomResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) StartMatch(context.Context, *connect.Request[v1.StartMatchRequest]) (*connect.Response[v1.StartMatchResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.StartMatchResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) GetQuestionCount(context.Context, *connect.Request[v1.GetQuestionCountRequest]) (*connect.Response[v1.GetQuestionCountResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.GetQuestionCountResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) GetRoom(context.Context, *connect.Request[v1.GetRoomRequest]) (*connect.Response[v1.GetRoomResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.GetRoomResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) GetUsersInRoom(context.Context, *connect.Request[v1.GetUsersInRoomRequest]) (*connect.Response[v1.GetUsersInRoomResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.GetUsersInRoomResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) CheckIfRoomExists(context.Context, *connect.Request[v1.CheckIfRoomExistsRequest]) (*connect.Response[v1.CheckIfRoomExistsResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.CheckIfRoomExistsResponse]), args.Error(1)
}

func (m *MockRoomsServiceClient) UserFinishedAnswering(context.Context, *connect.Request[v1.UserFinishedAnsweringRequest]) (*connect.Response[v1.UserFinishedAnsweringResponse], error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(*connect.Response[v1.UserFinishedAnsweringResponse]), args.Error(1)
}
