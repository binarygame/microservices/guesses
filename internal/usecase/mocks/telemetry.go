package mocks

import (
	"context"

	"github.com/stretchr/testify/mock"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
)

type MockTelemetryService struct {
	mock.Mock
}

type MockTelemetrySpan struct {
	mock.Mock
}

func (m *MockTelemetryService) NewCounter(name, description string, valueType telemetry.CounterType) error {
	args := m.Called(name, description, valueType)
	return args.Error(0)
}

func (m *MockTelemetryService) NewCounterUpDown(name, description string, valueType telemetry.CounterType) error {
	args := m.Called(name, description, valueType)
	return args.Error(0)
}

func (m *MockTelemetryService) NewHistogram(name, description string, valueType telemetry.HistogramType) error {
	args := m.Called(name, description, valueType)
	return args.Error(0)
}

func (m *MockTelemetryService) IncrementCounter(ctx context.Context, name string, value interface{}) {
	m.Called(ctx, name, value)
}

func (m *MockTelemetryService) IncrementCounterUpDown(ctx context.Context, name string, value interface{}) {
	m.Called(ctx, name, value)
}

func (m *MockTelemetryService) DecrementCounterUpDown(ctx context.Context, name string, value interface{}) {
	m.Called(ctx, name, value)
}

func (m *MockTelemetryService) Record(ctx context.Context, name string, properties map[string]string) telemetry.Span {
	args := m.Called(ctx, name, properties)
	return args.Get(0).(telemetry.Span)
}

func (m *MockTelemetrySpan) End() {
}

func (m *MockTelemetryService) ObserveValue(ctx context.Context, name string, value interface{}) {
	m.Called(ctx, name, value)
}
