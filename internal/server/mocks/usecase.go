package mocks

import (
	"context"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

type MockGuessesUsecase struct {
	mock.Mock
}

func (m *MockGuessesUsecase) Submit(ctx context.Context, roomID, userID, questionID string, correct bool, timeTaken time.Duration) error {
	args := m.Called(ctx, roomID, userID, questionID, correct, timeTaken)
	return args.Error(0)
}

func (m *MockGuessesUsecase) GetLeaderboard(ctx context.Context, roomID string) (*models.Leaderboard, error) {
	args := m.Called(ctx, roomID)
	if args.Get(0) != nil {
		return args.Get(0).(*models.Leaderboard), args.Error(1)
	}

	return nil, args.Error(1)
}

func (m *MockGuessesUsecase) ResetScores(ctx context.Context, roomID string, questionCount uint32) error {
	args := m.Called(ctx, roomID, questionCount)
	return args.Error(0)
}

func (m *MockGuessesUsecase) RecoverUserState(ctx context.Context, roomID, userID string) (*[]models.Guess, error) {
	args := m.Called(ctx, roomID, userID)
	if args.Get(0) != nil {
		return args.Get(0).(*[]models.Guess), args.Error(1)
	}

	return nil, args.Error(1)
}

func (m *MockGuessesUsecase) Subscribe(ctx context.Context, roomId string) (<-chan models.Event, error) {
	args := m.Called(ctx, roomId)
	if args.Get(0) != nil {
		return args.Get(0).(<-chan models.Event), args.Error(1)
	}

	return nil, args.Error(1)
}

func (m *MockGuessesUsecase) SubscribeToAll(ctx context.Context) (<-chan models.Event, error) {
	args := m.Called(ctx)
	if args.Get(0) != nil {
		return args.Get(0).(<-chan models.Event), args.Error(1)
	}

	return nil, args.Error(1)
}

func (m *MockGuessesUsecase) StartBackgroundTasks() {}
