#! /bin/sh

SCRIPT_PATH=$(dirname $(readlink -f $0))
PROTO_DIR=../api/proto/

# This script compiles the protobuf files in the correct location
cd "$SCRIPT_PATH/$PROTO_DIR"

# Run linter and store result
lint_result=$(buf lint)
# Check if lint result is not empty
if [ -n "$lint_result" ]; then
  # Print lint result
  echo -e "\n\033[0;91m-------- Lint failed! --------\033[0m\n$lint_result\n\033[0;91m------------------------------\033[0m"
fi

result=$(buf generate)
# Return to original path based 
cd - > /dev/null

# Check if buf ran successfully
if [ $? -ne 0 ]; then
  # Print error message to user with color
  echo -e "\n\033[0;91mError compiling the protobuf files!\033[0m\n"
  # Print error message from buf
  echo -e "\033[0;91m$result\033[0m\n"
  # Exit with error
  exit 1
else
    # Print nice message to user with color
    echo -e "\n\033[0;32mCompiled the protobuf files!\033[0m\n"
fi


