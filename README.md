# BinaryGame Guesses Microservice

[![Pipeline Status](https://gitlab.com/binarygame/microservices/guesses/badges/development/pipeline.svg?ignore_skipped=true)](https://gitlab.com/binarygame/microservices/guesses/badges/development/pipeline.svg)
[![Coverage Report](https://gitlab.com/binarygame/microservices/guesses/badges/development/coverage.svg)](https://gitlab.com/binarygame/microservices/guesses/badges/development/coverage.svg)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/binarygame/microservices/guesses)](https://goreportcard.com/report/gitlab.com/binarygame/microservices/guesses)
[![Latest Release](https://gitlab.com/binarygame/microservices/guesses/-/badges/release.svg?order_by=release_at)](https://gitlab.com/binarygame/microservices/guesses/-/badges/release.svg?order_by=release_at)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

The `guesses` service is a microservice responsible for handling and storing user guesses.

## Table of Contents

- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
- [Building Image Builds Locally](#building-image-builds-locally)
- [Deploy](#deploy)
- [Testing](#testing)
- [Contributing](#contributing)
- [License](#license)
- [Credits](#credits)

## Features

## Requirements

- Go 1.22+
- Valkey (an open-source distribution of Redis)

## Installation

1. Clone the repository:

   ```sh
   git clone https://gitlab.com/binarygame/microservices/guesses.git
   cd guesses
   ```

2. Install the dependencies:

   ```sh
   go mod tidy
   ```

## Configuration

Configure the service by setting the following environment variables:

- `GUESSES_SERVICE_HOST`: The host for the guesses service. Default: `0.0.0.0`.
- `GUESSES_SERVICE_PORT`: The port for the guesses service. Default: `8001`.
- `VALKEY_SERVICE_HOST`: The host for the Valkey service. Default: `localhost`.
- `VALKEY_SERVICE_PORT`: The port for the Valkey service. Default: `6379`.
- `LOG_LEVEL`: The logging level for the service. Default: `DEBUG`.
- `OTEL_COLLECTOR_HOST` (optional): The host for the OpenTelemetry collector. Default: `localhost`
- `OTEL_COLLECTOR_PORT` (optional): The port for the OpenTelemetry collector. Default: `4317`

You can set these variables in a `.env` file or export them directly in your shell.

## Usage

1. Run the development server:

   ```sh
   go run cmd/guesses/main.go
   ```

2. The service will be available at `http://localhost:8001`.

### Calling the API with `grpcurl`

You can use `grpcurl` to call the service. For example, to submit a guess:

```sh
grpcurl -plaintext -d '{"userId": "user123", "guess": "answer"}' localhost:8001 connectrpc.guesses.v1.GuessesService.SubmitGuess
```

## Building Image Builds Locally

This project makes use of [Earthly](https://earthly.dev/) to build container images and for local testing binaries.

Build project with binary output at `build/app`:

```bash
earthly +build
```

Build project with images as output:

```bash
# This command outputs the image names for your testing
earthly +build-image
```

## Deploy

On this project, [Earthly](https://earthly.dev/) is used to build and publish the images on Gitlab's built-in Container Registry.

The following tags are available from this system:

`latest` tags:

- `latest`: latest production build _(alias: `latest-production`)_
- `latest-staging`: latest staging build
- `latest-development`: latest development build (git version)
- `latest-mr-<Merge Request ID>`: latest build from a specific merge request.
  - _The merge request id is the internal variant._

Commit-specific tags:

- `<Short Commit Hash>-production`: Available for all commits in the production branch.
- `<Short Commit Hash>-staging`: Available for all commits in the staging branch.
- `<Short Commit Hash>-development`: Available for all commits in the development branch.

## Testing

To run the tests, use the following command:

```sh
go test ./...
```

Alternatively, you can use `gotestsum` to run the tests with summarized output:

```sh
go run gotest.tools/gotestsum@latest
```

Ensure you have the test dependencies installed.

## Contributing

1. Fork the repository.
2. Create a new branch: `git checkout -b feature/your-feature-name`.
3. Make your changes and commit them: `git commit -m 'Add some feature'`.
4. Push to the branch: `git push origin feature/your-feature-name`.
5. Open a pull request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Credits

- Project icon designed by `lutfix` from [Flaticon](https://www.flaticon.com/free-icon/question-and-answer_6854825).
