package models

import (
	"github.com/go-playground/validator/v10"
)

var modelValidator *validator.Validate = validator.New()

type Guess struct {
	RoomID     string `validate:"required,len=4" redis:"roomid"`
	UserID     string `validate:"required,uuid" redis:"userid"`
	QuestionID string `validate:"required,gte=0" redis:"questionid"`
	Correct    bool   `redis:"correct"`
}

func NewGuess(roomID, userID, questionID string, correct bool) (*Guess, error) {
	g := &Guess{
		RoomID:     roomID,
		UserID:     userID,
		QuestionID: questionID,
		Correct:    correct,
	}

	err := g.Validate()
	if err != nil {
		return nil, err
	}

	return g, nil
}

func (u *Guess) Validate() error {
	return modelValidator.Struct(u)
}
