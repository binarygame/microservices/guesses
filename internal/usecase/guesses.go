package usecase

import (
	"context"
	"log/slog"
	"strconv"
	"time"

	"connectrpc.com/connect"
	roomsv1 "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1"

	"gitlab.com/binarygame/microservices/guesses/pkg/constants"
	"gitlab.com/binarygame/microservices/guesses/pkg/errdefs"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
)

// Receives and store users answers data, for each question.
// Before updating users scores, it validates if they
// already answered the question (overwriting is not allowed).
// Finally, scores are updated in a progressive way with the `ComboMultiplier` array
func (gu *guesses) Submit(ctx context.Context, roomId, userId, questionId string, correct bool, timeTaken time.Duration) error {
	// New grouped logger
	timeTakenStr := timeTaken.String()
	logger := gu.logger.WithGroup("submit").With("roomId", roomId, "userId", userId, "questionsId", questionId, "correct", correct, "timeTaken", timeTakenStr)

	logger.LogAttrs(ctx, slog.LevelDebug, "Submit called")

	span := gu.telemetry.Record(ctx, "Submit", map[string]string{
		"roomID":     roomId,
		"userID":     userId,
		"questionID": questionId,
		"correct":    strconv.FormatBool(correct),
		"timeTaken":  timeTakenStr,
	})
	defer span.End()

	guess, err := models.NewGuess(roomId, userId, questionId, correct)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelDebug, "Guess validation failed", slog.String("error", err.Error()))
		return errdefs.ErrInvalidData
	}

	alreadyAnswered, err := gu.repository.UserAlreadyAnswered(ctx, guess)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Database failed on UserAlreadyAnswered", slog.String("error", err.Error()))
		return errdefs.ErrDatabaseFailed
	}

	if alreadyAnswered {
		logger.LogAttrs(ctx, slog.LevelDebug, "User already answered the question",
			slog.String("Room ID", roomId),
			slog.String("User ID", userId),
			slog.String("Question ID", questionId),
		)
		return errdefs.ErrUserAlreadyAnswered
	}

	// at this point, scores should be already initialized in the database
	score, err := gu.repository.GetUserScore(ctx, roomId, userId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Database failed on GetUserScore", slog.String("error", err.Error()))
		return errdefs.ErrDatabaseFailed
	}

	if score == nil {
		logger.LogAttrs(ctx, slog.LevelError, "Could not retrieve user score")
		return errdefs.ErrInternalServer
	}

	score.UpdateStatistics(correct, timeTaken)

	err = gu.repository.StoreGuess(ctx, guess, score)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Database failed on StoreGuess", slog.String("error", err.Error()))
		return errdefs.ErrDatabaseFailed
	}

	// ignore this for now
	// e1 := models.NewGuessEvent(models.GuessSubmitted, guess.RoomID, guess.UserID, guess.QuestionID, guess.Correct, "User submitted guess")
	// gu.repository.PublishEvent(ctx, e1)

	if correct {
		ldb, err := gu.repository.GetLeaderboard(ctx, roomId)
		if err != nil {
			logger.LogAttrs(ctx, slog.LevelError, "Could not retrieve room leaderboard", slog.String("error", err.Error()))
			return errdefs.ErrDatabaseFailed
		}

		e2 := models.NewLeaderboardUpdatedEvent(models.LeaderboardUpdated, guess.RoomID, ldb, "Leaderboard updated")
		gu.repository.PublishEvent(ctx, e2)
	}

	if score.WrongAnswers+score.CorrectAnswers == score.TotalQuestions {
		_, err := gu.roomsClient.UserFinishedAnswering(ctx, connect.NewRequest(&roomsv1.UserFinishedAnsweringRequest{RoomId: roomId, UserId: userId}))
		if err != nil {
			logger.LogAttrs(ctx, slog.LevelError, "Failed to notify room service that user finished answering", slog.String("error", err.Error()))
			return errdefs.ErrInternalServer
		}
	}

	gu.telemetry.IncrementCounter(ctx, constants.GuessesSubmittedCounterKey, 1)

	return nil
}

// Returns a room's sorted scores list
func (gu *guesses) GetLeaderboard(ctx context.Context, roomId string) (*models.Leaderboard, error) {
	// New grouped logger
	logger := gu.logger.WithGroup("get_leaderboard").With("roomId", roomId)

	logger.LogAttrs(ctx, slog.LevelDebug, "GetLeaderboard called")

	leaderboard, err := gu.repository.GetLeaderboard(ctx, roomId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Could not retrieve room leaderboard", slog.String("error", err.Error()))
		return nil, errdefs.ErrDatabaseFailed
	}

	return leaderboard, nil
}

// Initialize or reset users scores in a room
func (gu *guesses) ResetScores(ctx context.Context, roomId string, questionCount uint32) error {
	// New grouped logger
	logger := gu.logger.WithGroup("reset_scores").With("roomId", roomId)

	logger.LogAttrs(ctx, slog.LevelDebug, "ResetScores called")

	users, err := gu.roomsClient.GetUsersInRoom(
		ctx,
		connect.NewRequest(&roomsv1.GetUsersInRoomRequest{
			RoomId: roomId,
		}))
	if err != nil || users.Msg == nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to get users ids in room", slog.String("error", err.Error()))
		return errdefs.ErrInternalServer
	}

	err = gu.repository.ResetRoomScores(ctx, roomId, users.Msg.UserIds, questionCount)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Error resetting users score", slog.String("error", err.Error()))
		return errdefs.ErrDatabaseFailed
	}

	return nil
}

// In case an user's connection fails, this method
// returns the user guesses previous state
func (gu *guesses) RecoverUserState(ctx context.Context, roomId, userId string) (*[]models.Guess, error) {
	return nil, nil // TODO
}
